package tourGuide.service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.concurrent.CompletableFuture;

import org.springframework.stereotype.Service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.tinylog.Logger;
import tourGuide.dto.AllCurrentLocationsDTO;
import tourGuide.helper.InternalTestHelper;
import tourGuide.repository.GpsRepository;
import tourGuide.repository.UserRepository;
import tourGuide.tracker.Tracker;
import tourGuide.user.User;
import tourGuide.user.UserReward;

@Service
public class UsersService {
	private final RewardsService rewardsService;
	private UserRepository userRepository;
	private GpsRepository gpsRepository;
	public final Tracker tracker;
	boolean testMode = true;
	
	public UsersService(RewardsService rewardsService, UserRepository userRepository, GpsRepository gpsRepository) {
		this.rewardsService = rewardsService;
		this.userRepository = userRepository;
		this.gpsRepository = gpsRepository;
		
		if(testMode) {
			Logger.info("TestMode enabled");
			Logger.debug("Initializing users");
			initializeInternalUsers();
			Logger.debug("Finished initializing users");
		}
		tracker = new Tracker(this, rewardsService);
		addShutDownHook();
	}
	
	public List<UserReward> getUserRewards(User user) {
		Logger.debug("Getting user's reward");
		return user.getUserRewards();
	}
	
	public VisitedLocation getUserLocation(User user) {
		Logger.debug("Getting location");
		VisitedLocation visitedLocation = (user.getVisitedLocations().size() > 0) ?
			user.getLastVisitedLocation() :
			trackUserLocation(user);
		rewardsService.calculateRewardsAsync(Collections.singletonList(user));
		return visitedLocation;
	}
	
	public User getUser(String userName) {
		Logger.debug("Getting user");
		return userRepository.getByUsername(userName);
	}
	
	public List<User> getAllUsers() {
		Logger.debug("Getting all users");
		return userRepository.getAll();
	}
	
	public void addUser(User user) {
		Logger.debug("Adding user");
		userRepository.add(user);
	}
	
	public VisitedLocation trackUserLocation(User user) {
		Logger.debug("Tracking user");
		VisitedLocation visitedLocation = gpsRepository.getUserLocation(user.getUserId());
		user.addToVisitedLocations(visitedLocation);

		rewardsService.calculateRewardsAsync(Collections.singletonList(user));
		return visitedLocation;
	}

	public List<VisitedLocation> trackAllUserLocation() {
		Logger.debug("Tracking all users");
		List<User> users = getAllUsers();

		List<CompletableFuture<VisitedLocation>> userLocationFutures  = users.stream()
				.map(user -> CompletableFuture.supplyAsync(() -> {
					VisitedLocation visitedLocation = gpsRepository.getUserLocation(user.getUserId());
					user.addToVisitedLocations(visitedLocation);
					rewardsService.calculateRewardsAsync(Collections.singletonList(user));
					return visitedLocation;
				} ))
				.toList();

        return userLocationFutures .stream()
				.map(CompletableFuture::join)
				.toList();
	}

	public List<AllCurrentLocationsDTO> getAllUsersLocations() {
		Logger.debug("Getting all locations");
		List<User> users = getAllUsers();

		List<AllCurrentLocationsDTO> allCurrentLocationsDTOS = new ArrayList<AllCurrentLocationsDTO>();
		for (int i=0; i < users.size(); i++){
			AllCurrentLocationsDTO allCurrentLocationsDTO = new AllCurrentLocationsDTO();
			allCurrentLocationsDTO.setUserID(users.get(i).getUserId());
			allCurrentLocationsDTO.setLocation(users.get(i).getLastVisitedLocation().location);

			allCurrentLocationsDTOS.add(allCurrentLocationsDTO);
		}

		return allCurrentLocationsDTOS;
	}


	
	private void addShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() { 
		      public void run() {
		        tracker.stopTracking();
		      } 
		    }); 
	}
	
	/**********************************************************************************
	 * 
	 * Methods Below: For Internal Testing
	 * 
	 **********************************************************************************/

	// Database connection will be used for external users, but for testing purposes internal users are provided and stored in memory
	private void initializeInternalUsers() {
		IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
			String userName = "internalUser" + i;
			String phone = "000";
			String email = userName + "@tourGuide.com";
			User user = new User(UUID.randomUUID(), userName, phone, email);
			generateUserLocationHistory(user);

			userRepository.add(user);
		});
		Logger.debug("Created " + InternalTestHelper.getInternalUserNumber() + " internal test users.");
	}

	private void generateUserLocationHistory(User user) {
		IntStream.range(0, 3).forEach(i -> {
			user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime()));
		});
	}

	private double generateRandomLongitude() {
		double leftLimit = -128;
		double rightLimit = -66;
		return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
	}

	private double generateRandomLatitude() {
		double leftLimit = 24;
		double rightLimit = 51;
		return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
	}

	private Date getRandomTime() {
		LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
		return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
	}
}
