package tourGuide.repository;

import org.springframework.stereotype.Repository;
import org.tinylog.Logger;
import tourGuide.user.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class UserRepository{

    List<User> userList = new ArrayList<>();

    public List<User> getAll() {
        return userList;
    }

    public void add(User user) {
        if(!userList.contains(user)) {
            userList.add(user);
        }
    }

    public User getByUsername(String userName) {
        for (User user: userList) if(user.getUserName().equals(userName)) return user;
        return null;
    }

    public void deleteById(UUID id) {
        userList.removeIf(user -> user.getUserId().equals(id));
    }

    //For testing purpose only
    public void deleteAll() {
        userList.clear();
    }
}
