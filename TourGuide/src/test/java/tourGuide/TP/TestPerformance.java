package tourGuide.TP;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


import org.apache.commons.lang3.time.StopWatch;


import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import rewardCentral.RewardCentral;
import tourGuide.helper.InternalTestHelper;
import tourGuide.repository.GpsRepository;
import tourGuide.repository.UserRepository;
import tourGuide.service.RewardsService;
import tourGuide.service.UsersService;
import tourGuide.user.User;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
public class TestPerformance {

    /*
     * A note on performance improvements:
     *
     *     The number of users generated for the high volume tests can be easily adjusted via this method:
     *
     *     		InternalTestHelper.setInternalUserNumber(100000);
     *
     *
     *     These tests can be modified to suit new solutions, just as long as the performance metrics
     *     at the end of the tests remains consistent.
     *
     *     These are performance metrics that we are trying to hit:
     *
     *     highVolumeTrackLocation: 100,000 users within 15 minutes:
     *     		assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
     *
     *     highVolumeGetRewards: 100,000 users within 20 minutes:
     *          assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
     */

    @Test
    @Tag("performance")
    public void highVolumeTrackLocation() {
        GpsUtil gpsUtil = new GpsUtil();
        GpsRepository gpsRepository = new GpsRepository(gpsUtil);
        UserRepository userRepository = new UserRepository();
        RewardsService rewardsService = new RewardsService(gpsRepository, new RewardCentral());
        InternalTestHelper.setInternalUserNumber(100000);
        UsersService usersService = new UsersService(rewardsService, userRepository, gpsRepository);

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        usersService.trackAllUserLocation();
        stopWatch.stop();
        usersService.tracker.stopTracking();

        System.out.println("highVolumeTrackLocation: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
        assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
    }

    @Test
    @Tag("performance")
    public void highVolumeGetRewards() {
        GpsUtil gpsUtil = new GpsUtil();
        GpsRepository gpsRepository = new GpsRepository(gpsUtil);
        UserRepository userRepository = new UserRepository();
        RewardsService rewardsService = new RewardsService(gpsRepository, new RewardCentral());
        InternalTestHelper.setInternalUserNumber(100000);
        UsersService usersService = new UsersService(rewardsService, userRepository, gpsRepository);

        StopWatch stopWatch = new StopWatch();


        stopWatch.start();
        Attraction attraction = gpsRepository.getAll().get(0);
        List<User> allUsers = usersService.getAllUsers();
        allUsers.forEach(u -> u.addToVisitedLocations(new VisitedLocation(u.getUserId(), attraction, new Date())));

        rewardsService.calculateRewardsAsync(allUsers);

        for (User user : allUsers) {
            assertFalse(user.getUserRewards().isEmpty());
        }
        stopWatch.stop();
        usersService.tracker.stopTracking();

        System.out.println("highVolumeGetRewards: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
        assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
    }

}
