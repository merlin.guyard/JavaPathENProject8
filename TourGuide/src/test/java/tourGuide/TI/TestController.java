package tourGuide.TI;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.util.*;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import tourGuide.repository.GpsRepository;
import tourGuide.repository.UserRepository;
import tourGuide.service.RewardsService;
import tourGuide.service.UsersService;
import tourGuide.user.User;
import tourGuide.user.UserReward;

@SpringBootTest
@AutoConfigureMockMvc
public class TestController {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GpsRepository gpsRepository;

    @Autowired
    private UsersService usersService;

    @Autowired
    private RewardsService rewardsService;

    @BeforeEach
    public void setUp() {
        // Supprimer toutes les entités avant chaque test
        userRepository.deleteAll();
    }

    @Test
    public void index() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is("Greetings from TourGuide!")));

    }

    @Test
    public void getLocations() throws Exception {
        //Arrange
        User user = new User(
                UUID.randomUUID(),
                "bob",
                "0601010101",
                "Bob@mail.com"
        );
        user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(5.0, 10.0), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").parse("2023-09-07T10:11:12.000+00:00")));
        userRepository.add(user);

        //Act
        mockMvc.perform(get("/getLocation")
                        .param("userName", user.getUserName()))

                //Assert
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.location.latitude", is(5.0)))
                .andExpect(jsonPath("$.location.longitude", is(10.0)))
                .andExpect(jsonPath("$.timeVisited", is("2023-09-07T10:11:12.000+00:00")));
    }


    @Test
    public void getNearbyAttractions() throws Exception {
        //Arrange
        User user = new User(
                UUID.randomUUID(),
                "bob",
                "0601010101",
                "Bob@mail.com"
        );
        user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(33.817595, -117.922008), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").parse("2023-09-07T10:11:12.000+00:00")));
        userRepository.add(user);

        //Act
        mockMvc.perform(get("/getNearbyAttractions")
                        .param("userName", user.getUserName()))

                //Assert
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name", is("Disneyland")))
                .andExpect(jsonPath("$[0].attractionLongitude", is(-117.922008)))
                .andExpect(jsonPath("$[0].userLongitude", is(-117.922008)))
                .andExpect(jsonPath("$[0].distance", is(0.0)))
                .andExpect(jsonPath("$[0].rewardPoints", both(greaterThanOrEqualTo(1)).and(lessThanOrEqualTo(1000))));
    }

    @Test
    public void getRewards() throws Exception {
        //Arrange
        User user = new User(
                UUID.randomUUID(),
                "bob",
                "0601010101",
                "Bob@mail.com"
        );
        Attraction attraction = gpsRepository.getAll().get(0);
        user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(33.817595, -117.922008), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").parse("2023-09-07T10:11:12.000+00:00")));
        user.addUserReward(new UserReward(user.getLastVisitedLocation(), attraction, 5));
        userRepository.add(user);

        //Act
        mockMvc.perform(get("/getRewards")
                        .param("userName", user.getUserName()))

                //Assert
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].visitedLocation.userId", is(user.getUserId().toString())))
                .andExpect(jsonPath("$[0].visitedLocation.location.longitude", is(-117.922008)))
                .andExpect(jsonPath("$[0].attraction.longitude", is(-117.922008)))
                .andExpect(jsonPath("$[0].attraction.attractionName", is("Disneyland")))
                .andExpect(jsonPath("$[0].attraction.attractionId", is(attraction.attractionId.toString())))
                .andExpect(jsonPath("$[0].rewardPoints", is(5)));
    }

    @Test
    public void getAllCurrentLocations() throws Exception {
        //Arrange
        User user = new User(
                UUID.randomUUID(),
                "bob",
                "0601010101",
                "Bob@mail.com"
        );
        user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(33.817595, -117.922008), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").parse("2023-09-07T10:11:12.000+00:00")));
        userRepository.add(user);
        //Act
        mockMvc.perform(get("/getAllCurrentLocations"))
                //Assert
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    public void getTripDeals() throws Exception {
//Arrange
        User user = new User(
                UUID.randomUUID(),
                "bob",
                "0601010101",
                "Bob@mail.com"
        );
        user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(33.817595, -117.922008), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").parse("2023-09-07T10:11:12.000+00:00")));
        userRepository.add(user);

        //Act
        mockMvc.perform(get("/getTripDeals")
                        .param("userName", user.getUserName()))
                //Assert
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)));
    }
}