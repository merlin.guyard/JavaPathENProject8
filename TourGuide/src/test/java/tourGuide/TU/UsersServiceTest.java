package tourGuide.TU;


import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tourGuide.repository.GpsRepository;
import tourGuide.repository.UserRepository;
import tourGuide.service.UsersService;
import tourGuide.user.User;
import tourGuide.user.UserReward;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class UsersServiceTest {

    @Autowired
    UsersService usersService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    GpsRepository gpsRepository;

    @BeforeEach
    public void setUp() {
        // Supprimer toutes les entités avant chaque test
        userRepository.deleteAll();
    }

    @Test
    public void getUserTest() {
        //Arrange
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        usersService.addUser(user);

        //Act
        User user2check = usersService.getUser(user.getUserName());

        //Assert
        assertEquals(user2check.getUserName(), user.getUserName());
    }

    @Test
    public void getAllUsersTest() {
        //Arrange
        User user1 = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        User user2 = new User(UUID.randomUUID(), "bob", "000", "bob@tourGuide.com");
        usersService.addUser(user1);
        usersService.addUser(user2);

        //Act
        List<User> users2check = usersService.getAllUsers();

        //Assert
        assertEquals(users2check.get(0).getUserName(), user1.getUserName());
        assertEquals(users2check.get(1).getUserName(), user2.getUserName());
        assertEquals(users2check.size(),2);
    }

    @Test
    public void getUserLocationTest() throws Exception {
        //Arrange
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(5.0, 10.0), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").parse("2023-09-07T10:11:12.000+00:00")));
        usersService.addUser(user);

        //Act
        User user2check = usersService.getUser(user.getUserName());

        //Assert
        assertEquals(user2check.getLastVisitedLocation().location.latitude, 5.0);
    }

    @Test
    public void getUserRewardsTest() throws Exception {
        //Arrange
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        Attraction attraction = gpsRepository.getAll().get(0);
        user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(5.0, 10.0), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").parse("2023-09-07T10:11:12.000+00:00")));
        user.addUserReward(new UserReward(user.getLastVisitedLocation(), attraction, 5));
        usersService.addUser(user);

        //Act
        User user2check = usersService.getUser(user.getUserName());

        //Assert
        assertEquals(user2check.getUserRewards().get(0).attraction.attractionName, attraction.attractionName);
    }

    @Test
    public void getAllUsersLocationsTest() throws Exception {
        //Arrange
        User user1 = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        User user2 = new User(UUID.randomUUID(), "bob", "000", "bob@tourGuide.com");
        Attraction attraction = gpsRepository.getAll().get(0);
        user1.addToVisitedLocations(new VisitedLocation(user1.getUserId(), new Location(5.0, 10.0), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").parse("2023-09-07T10:11:12.000+00:00")));
        user2.addToVisitedLocations(new VisitedLocation(user2.getUserId(), new Location(15.0, 10.0), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").parse("2023-09-07T10:11:12.000+00:00")));
        usersService.addUser(user1);
        usersService.addUser(user2);

        //Act
        List<User> users2check = usersService.getAllUsers();

        //Assert
        assertEquals(users2check.get(0).getLastVisitedLocation().location.latitude, 5.0);
        assertEquals(users2check.get(1).getLastVisitedLocation().location.latitude, 15.0);
        assertEquals(users2check.size(),2);
    }

    @Test
    public void trackUserLocationTest() throws Exception {
        //Arrange
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        Attraction attraction = gpsRepository.getAll().get(0);
        user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(5.0, 10.0), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").parse("2023-09-07T10:11:12.000+00:00")));
        user.addUserReward(new UserReward(user.getLastVisitedLocation(), attraction, 5));
        usersService.addUser(user);

        //Act
        VisitedLocation visitedLocation = usersService.trackUserLocation(user);

        //Assert
        assertEquals(user.getUserId(), visitedLocation.userId);
    }
}
