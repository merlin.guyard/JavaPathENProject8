package tourGuide.TU;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import rewardCentral.RewardCentral;
import tourGuide.dto.NearbyAttractionsDTO;
import tourGuide.helper.InternalTestHelper;
import tourGuide.repository.GpsRepository;
import tourGuide.repository.UserRepository;
import tourGuide.service.RewardsService;
import tourGuide.service.UsersService;
import tourGuide.user.User;
import tourGuide.user.UserReward;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class RewardsServiceTest {

    @Autowired
    RewardsService rewardsService;

    @Autowired
    UsersService usersService;

    @Autowired
    GpsRepository gpsRepository;

    @Autowired
    UserRepository userRepository;

    @Test
    public void get5NearbyAttractionsTest() throws Exception {
        //Arrange
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(33.817595, -117.922008), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").parse("2023-09-07T10:11:12.000+00:00")));
        usersService.addUser(user);

        //Act
        List<NearbyAttractionsDTO> attractions = rewardsService.get5NearestAttractions(user);

        //Assert
        assertEquals(attractions.get(0).getName(), "Disneyland");
        assertEquals(5, attractions.size());
    }


    @Test
    public void calculateRewardsAsync() throws Exception {
        //Arrange
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(33.817595, -117.922008), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").parse("2023-09-07T10:11:12.000+00:00")));
        usersService.addUser(user);

        //Act
        rewardsService.calculateRewardsAsync(Collections.singletonList(user));

        //Assert
        assertFalse(user.getUserRewards().isEmpty());
    }
}
