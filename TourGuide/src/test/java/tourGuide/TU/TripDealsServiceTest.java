package tourGuide.TU;

import gpsUtil.GpsUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import rewardCentral.RewardCentral;
import tourGuide.helper.InternalTestHelper;
import tourGuide.repository.GpsRepository;
import tourGuide.repository.UserRepository;
import tourGuide.service.RewardsService;
import tourGuide.service.TripDealsService;
import tourGuide.service.UsersService;
import tourGuide.user.User;
import tripPricer.Provider;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class TripDealsServiceTest {

    @Autowired
    TripDealsService tripDealsService;

    @Autowired
    UsersService usersService;

    @Autowired
    UserRepository userRepository;

    @Test
    public void getTripDeals() {
        //Arrange
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        usersService.addUser(user);

        //Act
        List<Provider> providers = tripDealsService.getTripDeals(user);

        //Assert
		assertEquals(providers.size(), 5);
    }
}
